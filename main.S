; Christmas tree
;
; Copyright (c) 2021 AnyKeyShik Rarity <nikitav59@gmail.com>

[bits 16]
[org 0x7c00]

section .text
    cli
    jmp 0:.main_segment

.main_segment:
    xor ax, ax
    mov dx, ax
    mov es, ax
    mov ss, ax
    mov sp, 0x7c00
    cld
    sti

    ; Clear screen
    push es
    mov ax, 0xb800
    mov es, ax
    xor di, di
    mov cx, 80*25
    mov ax, 0x0f20  ; Space, Foreground white, Background black
    rep stosw
    pop es

    ; Install IRQ handler
    cli
    mov ax, [8*4]
    mov [chained_irq0], ax
    mov ax, [8*4 + 2]
    mov [chained_irq0 + 2], ax
    mov word [8*4], irq0
    mov word [8*4 + 2], 0
    sti

    ; Change into VGA mode 0x13 (320x200, 256 colors).
    mov ax, 0x0013
    int 0x10

.start:
    ; Keep current tick in bx
    mov bx, [timer_counter]

    ; Clear the buffer
    mov ax, 0x9000
    mov es, ax
    xor di, di
    xor ax, ax
    mov cx, 320*200 / 2
    rep stosw

    ; Trace the parametric tree curve
    mov cx, 0

    ; Draw tree
    fldz
    fstp qword [tree_t]

    fldz
.treedraw:
    cmp cx, 1200
    je .treedone

    ; Compute t
	fld qword [tree_t]
    fld qword [tree_inc]
    faddp st1, st0
    fstp qword [tree_t]

	; Compute width(t)
	fld qword [tree_t]
    fld qword [tree_width_factor]
    fmulp st1, st0 ; width_factor*t = width(t)
    fstp qword [tree_width]

	; Compute tree_angle
	fld qword [tree_t]
    fld qword [tree_w]
    fmulp st1, st0 ; w*t
    fld qword [tree_p]
    fild dword [timer_counter]
    fmulp st1, st0 ; tick*p
    faddp st1, st0 ; w*t + tick*p
    fstp qword [tree_angle]

	; Compute X
	fld qword [tree_angle]
    fcos  ; st0 = cos(w*t + tick*p)
    fld qword [tree_width]
    fmulp st1, st0 ; st0 = width(t) * cos(...)
    fld qword [tree_width_base]
    faddp st1, st0 ; st0 = width_base + width(t) * cos(...)
    fstp qword [tree_x]

	; Compute Y
	fld qword [tree_t]
    fld qword [tree_height_factor]
    fmulp st1, st0
    fld qword [tree_height_base]
    faddp st1, st0
    fstp qword [tree_y]

	; Compute Z
	fld qword [tree_angle]
    fsin
    fld qword [tree_width]
    fmulp st1, st0
    fstp qword [tree_z]

	; Compute projected 2D coord
	fld qword [tree_x]
    fld qword [tree_z]
    fld qword [tree_x_z_factor]
    fmulp st1, st0
    faddp st1, st0
    fistp word [tree_disp_x]

    fld qword [tree_y]
    fld qword [tree_z]
    fld qword [tree_y_z_factor]
    fmulp st1, st0
    faddp st1, st0
    fistp word [tree_disp_y]

	; Canvas
	cmp word [tree_disp_y], 200
    jae .clipped
    cmp word [tree_disp_x], 320
    jae .clipped

	; Compute pixel address
	mov ax, [tree_disp_y]
    mov dx, 320
    mul dx
    add ax, [tree_disp_x]
    mov di, ax

	; Compute color
	mov ax, cx
    and ax, 1
    shl ax, 1
    add ax, 2  ; ax = 2 (green) or 4 (red)

	mov [es:di], al

.clipped:
	inc cx
    jmp .treedraw

.treedone:
	; Copy the double-buffer into the VGA framebuffer.
    push ds
    push es
    mov ax, 0x9000
    mov ds, ax
    mov ax, 0xa000
    mov es, ax
    mov si, 0
    mov di, 0
    mov cx, 320*200 / 2
    rep movsw
    pop es
    pop ds

.wait_next_tick:
    cmp bx, [timer_counter]
    jnz .start
    jmp .wait_next_tick

irq0:
    push word 0
    push word 0
    push ds
    push ax
    push bx
    mov bx, sp

	; Increment counter
	mov ax, 0
    mov ds, ax
    add word [timer_counter], 1
    adc word [timer_counter + 2], 0

	; Load the chained handler pointer
	mov ax, [chained_irq0]
    mov [bx + 6], ax
    mov ax, [chained_irq0 + 2]
    mov [bx + 8], ax

	pop bx
    pop ax
    pop ds

	; Return to the chained handler
    retf

    align 4
	chained_irq0: dw 0, 0
	timer_counter: dd 0

	; Floating-point constant pool
	tree_inc: dq 0.001
	tree_width_base: dq 160.0
	tree_width_factor: dq 100.0
	tree_height_base: dq 10.0
	tree_height_factor: dq 180.0
	tree_w: dq 31.4159265
	tree_p: dq 0.1745
	tree_x_z_factor: dq 0.5
	tree_y_z_factor: 
		dq 0.25
    	
	times 510-($-$$) db 0
    db 0x55, 0xaa

	; Parametric curve results
	tree_t: dq 0
	tree_width: dq 0
	tree_height: dq 0
	tree_angle: dq 0
	tree_x: dq 0
	tree_y: dq 0
	tree_z: dq 0
	tree_disp_x: dw 0
	tree_disp_y: dw 0
