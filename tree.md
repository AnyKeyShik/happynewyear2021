# Draw

### Tree draw logic

```
tree: (x,y,z) = (160 + width(t)*cos(w*t+p), height(t), width(t)*sin(w*t+p))
      width(t) = 140*t
      height(t) = 10 + 180*t
      w = 2*pi*5 = 31.415...
      p = 2*pi * tick/36 (0.5 revs/sec) = tick * 0.1745

project (x,y,z) into (x,y) with: x_disp = x + a_x*z, y_disp = y + a_y*z
                                   where a_x = 0.5, a_y = 0.25
```

### Color
```
color: alternate between 0x02 (green) and 0x04 (red).
```
