CC=nasm
CFLAGS=-fbin -o
SRC=main.S
OBJ=tree.obj
IMG=tree.img

.PHONY: default new_year clean

default: new_year

new_year: $(IMG)
	@qemu-system-x86_64 -fda $(IMG) 2>/dev/null
	@echo -e "\n\n\033[1;32mHappy New Year!\033[0m"

clean:
	@rm -f $(OBJ) $(IMG)

$(OBJ): $(SRC)
	@$(CC) $(CFLAGS) $(OBJ) $(SRC)
	@echo "Build object"

$(IMG): $(OBJ)
	@rm -f $(IMG)
	@dd if=/dev/zero of=$(IMG) bs=1024 count=1440
	@dd if=$(OBJ) of=$(IMG) conv=notrunc
	@echo "Build image"
